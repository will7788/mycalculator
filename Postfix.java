public class Postfix
{
    public double eval(String expr)
    {
        String[] tokens = expr.split(" ");
        Stack<Double> s = new Stack<Double>();
        
        for (int i = 0; i < tokens.length; i++)
        {
            String token = tokens[i];
            switch(token)
            {
                case "+":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b + a);
                    break;
                    }
                case "*":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b * a);
                    break;
                    }
                case "-":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b - a);
                    break;
                    }
                case "/":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(b / a);
                    break;
                    }
                case "^":
                    {
                    double a = s.pop();
                    double b = s.pop();
                    s.push(Math.pow(b, a));
                    break;
                    }
                case "pi":
                    {
                    s.push(Math.PI);
                    break;
                    }
                case "cos":
                    {
                    s.push(Math.cos(s.pop()));
                    break;
                    }
                case "sin":
                    {
                    s.push(Math.sin(s.pop()));
                    break;
                    }
                case "tan":
                    {
                    s.push(Math.tan(s.pop()));
                    break;
                    }
                default:
                    {
                        try
                        {
                            s.push(Double.parseDouble(token));
                            break;
                        }
                        catch(java.lang.NumberFormatException e)
                        {
                            throw new InvalidTokenException();
                        }
                    }
            
            /*
            if (token.equals("add"))
            {
                double a = s.pop();
                double b = s.pop();
                s.push(b + a);
            }
            else if (token.equals("x"))
            {
                double a = s.pop();
                double b = s.pop();
                s.push(b * a);
            }
            else if (token.equals("-"))
            {
                double a = s.pop();
                double b = s.pop();
                s.push(b - a);
            }
            else if (token.equals("^"))
            {
                double a = s.pop();
                double b = s.pop();
                s.push(Math.pow(b, a));
            }
            else   // Must be a number
            {
                s.push(Double.parseDouble(token));
            }
            */
        }
    }
    return s.peek();
}
}